const jwt = require("jsonwebtoken");

const config = require("../config");

const verifyToken = (req, res, next) => {
  const token =
    req.body.token || req.query.token || req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }
  try {
    const decoded = jwt.verify(token, config.TOKEN_KEY);
    req.user = decoded;
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }
  return next();
};

const verifyTokenSocket = (socket, next) => {
  const token = socket.handshake.query.token;

  if (!token) {
    return next(new Error("A token is required for authentication"));
  }
  try {
    const decoded = jwt.verify(token, config.TOKEN_KEY);
    console.log("Authorization:", decoded);
    socket.user = decoded;
    next();
  } catch (err) {
    console.log("Invalid token");
    return next(new Error("Invalid Token"));
  }
};

module.exports = { verifyToken, verifyTokenSocket };
