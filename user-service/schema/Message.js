const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const messageSchema = new Schema({
  messageId: {
    type: String,
    required: true,
    unique: true,
  },
  dateCreated: {
    type: Date,
    default: Date.now,
  },
  username: {
    required: true,
    type: String,
  },
  roomId: {
    required: true,
    type: String,
  },
  message: {
    required: true,
    type: String,
  },
  messageType: {
    required: true,
    type: String,
  },
  seen: {
    required: true,
    type: Boolean,
  },
});

// userSchema.index({ user_id: 1, user_id_2: 1 }, { unique: true });

module.exports = mongoose.model("Message", messageSchema, "Message");
