const express = require("express");
const router = express.Router();
const User = require("../schema/User");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const config = require("../config");
const os = require("os");
router.post("/register", async (req, res, next) => {
  try {
    let user = await User.findOne({
      email: req.body.email,
    });

    if (user) {
      return res.status(400).json({
        success: false,
        error: "Бүртгэлтэй хаяг байна",
      });
    } else {
      let hashedPassword = bcrypt.hashSync(req.body.password, 10);
      let newUser = new User({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: hashedPassword,
      });

      await newUser.save().catch((err) => {
        console.log(err);
        return res.status(500).json({
          success: false,
          error: "Бүртгэхэд асуудал гарлаа",
        });
      });

      return res.status(200).json({
        success: true,
        msg: "Амжилттай бүртгүүллээ",
        os: `${os.hostname()}`,
      });
    }
  } catch (error) {
    return res.status(400).json({ success: false, msg: error });
  }
});

router.post("/login", async (req, res, next) => {
  try {
    const user = await User.findOne({
      email: req.body.email,
    }).lean();

    if (!user || !bcrypt.compareSync(req.body.password, user.password)) {
      // authentication failed
      return res.status(400).json({
        success: false,
        msg: "Таны нэвтрэх нэр эсвэл нууц үг буруу байна",
      });
    } else {
      // authentication successful
      const token = jwt.sign({ username: user.username }, config.TOKEN_KEY, {
        expiresIn: "2h",
      });

      delete user.password;
      delete user._id;
      delete user.__v;

      user.token = token;
      return res.status(200).json({
        success: true,
        msg: "Амжилттай нэвтэрлээ",
        user,
        os: `${os.hostname()}`,
      });
    }
  } catch (error) {
    console.log(error);
    return res
      .status(400)
      .json({ success: false, msg: "Нэвтрэхэд асуудал гарлаа" });
  }
});

router.post("/search", async (req, res, next) => {
  try {
    console.log(req.body);

    let searchedUser = await User.find({
      username: { $regex: ".*" + req.body.keyword + ".*", $options: "i" },
    });
    console.log(searchedUser);

    return res.status(200).json({
      success: true,
      msg: "User found",
      users: searchedUser,
    });
  } catch (error) {
    return res.status(400).json({ success: false, msg: error });
  }
});

module.exports = router;
