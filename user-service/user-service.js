const express = require("express");
const bodyParser = require("body-parser");
const CONFIG = require("./config");
const app = express();
const cors = require("cors");
const userRouter = require("./routers/user");
const roomRouter = require("./routers/room");
const mongoose = require("mongoose");
const morgan = require("morgan");
const path = require("path");
const rfs = require("rotating-file-stream");

// const whitelist = ["http://localhost:8000", `${CONFIG.FRONTEND}`];

// const corsOptions = {
//   origin: function (origin, callback) {
//     if (whitelist.indexOf(origin) !== -1) {
//       return callback(null, true);
//     } else {
//       return callback(new Error("Not allowed by CORS"));
//     }
//   },
// };

// if (process.env.NODE_ENV == "dev") {
app.use(cors());
// } else {
//   app.use(cors(corsOptions));
// }

// app body parser by url and json
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(bodyParser.json({ limit: "50mb", type: "*/*" }));

const accessLogStream = rfs.createStream("access.log", {
  interval: "1d", // rotate daily
  path: path.join(__dirname, "log"),
});
// setup the logger
app.use(morgan("combined", { stream: accessLogStream }));

app.use("/user", userRouter);
app.use("/user", roomRouter);

app.get("/", (req, res) => res.send("Chat application demo server"));

//connect to db
mongoose
  .connect(CONFIG.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(console.log("DB connected"))
  .catch(() => console.log("db conn error"));

const httpServer = require("http").createServer(app);

// starting chat user registration service
httpServer.listen(CONFIG.USER_SERVICE_PORT, () =>
  console.log(`server running on ${CONFIG.USER_SERVICE_PORT} .`)
);
