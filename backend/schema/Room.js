const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const roomSchema = new Schema({
  roomId: {
    required: true,
    type: String,
    unique: true,
  },
  roomName: {
    required: true,
    type: String,
  },
  roomPhoto: {
    type: String,
  },
  lastUpdated: {
    required: true,
    type: Date,
    default: Date.now,
  },
  lastMessage: {
    type: String,
    required: true,
  },
  isPrivateChat: {
    type: Boolean,
  },
  users: {
    type: [
      {
        username: {
          type: String,
          required: true,
        },
        nickname: {
          type: String,
          required: false,
        },
      },
    ],
  },
});

module.exports = mongoose.model("Room", roomSchema, "Room");
