const express = require("express");
const bodyParser = require("body-parser");
const CONFIG = require("./config");
const app = express();
const cors = require("cors");
const amqp = require("amqplib/callback_api");
const User = require("./schema/User");
const Message = require("./schema/Message");
const Room = require("./schema/Room");
const mongoose = require("mongoose");
const morgan = require("morgan");
const { allowedNodeEnvironmentFlags } = require("process");
const auth = require("./middleware/auth");
const { json } = require("express");
const os = require("os");
console.log("waiting for rabbitmq server connection");

//connect to db
mongoose.connect(CONFIG.MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
}).then(console.log("DB connected"))
.catch(() => console.log("db conn error"));

const httpServer = require("http").createServer(app);

// starting chat socket server
httpServer.listen(CONFIG.SOCKET_SERVICE_PORT, () =>
  console.log(
    `server running on ${CONFIG.SOCKET_SERVICE_PORT} in ${process.env.NODE_ENV} environment.`
  )
);

createRabbitmqConnection()
  .then((rabbitConnection) => {
    return Promise.all([
      createRabbitChannel(rabbitConnection),
      createRabbitChannel(rabbitConnection),
    ]);
  })
  .then(([channelProducer, channelConsumer]) => {
    console.log("Connected to RabbitMQ");

    const ioServer = require("socket.io")(httpServer, {
      cors: {
        origin: '*',
      },
      allowUpgrade: false,
      transports: ["websocket"],
    });

    const EXCHANGE_NAME = "topic_logs";
    const EXCHANGE_TYPE = "topic";

    channelProducer?.assertExchange(EXCHANGE_NAME, EXCHANGE_TYPE, {
      durable: true,
      persist: true,
    });

    // non-durable queue with a generated name
    channelConsumer.assertQueue(
      "",
      {
        exclusive: true,
      },
      function (error2, q) {
        if (error2) {
          throw error2;
        }
        console.log(
          " [*] Waiting for messages in %s. To exit press CTRL+C",
          q.queue
        );
        channelConsumer.bindQueue(
          q.queue,
          EXCHANGE_NAME,
          "conversation.outgoing"
        );

        channelConsumer.consume(
          q.queue,
          (payload) => {
            const message = payload.content.toString();
            const messageObject = JSON.parse(message);
            console.log("consuming from queue %s", messageObject);

            if (payload.content) {
              console.log("sending to socket");

              // const clients = ioServer.sockets.adapter.rooms.get(
              //   messageObject.roomId
              // );
              // console.log("clients", clients);
              // for (const clientId of clients) {
              //   console.log(clientId);
              // }
              console.log(`consumed from pod: ${os.hostname()}`);
              return ioServer
                .to(messageObject.roomId)
                .emit("message:receive", message);
            }
          },
          {
            noAck: true, // үнэн үед consumer хүлээн авсны дараа queue-ээс хасна
          }
        );

        // channelConsumer.bindQueue(q.queue, EXCHANGE_NAME, "conversation.seen");

        // channelConsumer;
      }
    );

    // socket io user authorization middleware
    ioServer.use(auth.verifyTokenSocket);

    // starting socket.io server
    ioServer.on("connection", async (socket) => {
      console.log(socket.user.username, "-------------------------");
      let user = await User.findOne({
        username: socket.user.username,
      });
      // let user = await User.find();
      console.log("user", user);
      socket.join(user.roomId);

      console.log("user connected", socket.id, socket.handshake.query);

      Room.find({ "users.username": socket.user.username }, (err, rooms) => {
        const payload = JSON.stringify(rooms);
        console.log(payload);
        socket.emit("rooms:receive", payload);
      });

      socket.on("messages:get", async (roomId) => {
        console.log("sending chat conversation", roomId, os.hostname());
        let messages = await Message.find({ roomId });
        let payload = JSON.stringify(messages);
        console.log("chat conver", messages);
        socket.emit("messages:receive", payload);
      });

      // sending to queue
      socket.on("message:send", async (payload) => {
        console.log("---------------------------");
        const ROUTING_KEY = "conversation.outgoing";
        try {
          // Create a room if it doesn't exist
          let message = JSON.parse(payload);
          const room = await Room.findOne({ roomId: message.roomId });

          if (!room) {
            console.log("message", message);
            const newRoom = new Room({
              roomId: message.roomId,
              roomName: message.roomName,
              username: socket.user.username,
              lastMessage: message.message,
              users: [
                { username: message.roomName },
                { username: socket.user.username },
              ],
            });
            await newRoom.save();

            await User.findOneAndUpdate(
              { username: socket.user.username },
              {
                $push: {
                  roomId: message.roomId,
                },
              }
            );

            socket.join(message.roomId);
          }
          console.log("client rooms:", socket.rooms);

          console.log(`sent from pod: ${os.hostname()}`);
          message.from = os.hostname();
          console.log("pushing to queue: ", message);

          // TO-DO: save to db
          const document = new Message(message);
          document.save();
          console.log("msgs", message.message);
          await Room.updateOne(
            { roomId: message.roomId },
            { lastMessage: message.message }
          );

          payload = JSON.stringify(message)
          channelProducer.publish(
            EXCHANGE_NAME,
            ROUTING_KEY,
            Buffer.from(payload)
          );
        } catch (err) {
          console.log("aldaa", err);
          socket.emit("message:error", err);
        }
      });

      socket.on("seenMessage", (lastSeenMessage) => {
        console.log("seenMessage", lastSeenMessage);
        channelProducer.publish(
          EXCHANGE_NAME,
          "conversation.seen",
          Buffer.from(lastSeenMessage)
        );
        // socket.to("roomA").emit("seenMessageA", lastSeenMessage);
      });

      socket.on("disconnecting", () => {
        console.log("user disconnecting", socket.id);
      });
    });
  });

function createRabbitmqConnection() {
  return new Promise((resolve, reject) => {
    return amqp.connect(
      CONFIG.RABBIT_MQ.CONNECTION_URL,
      function (error0, connection) {
        if (error0) {
          reject(error0);
        }

        resolve(connection);
      }
    );
  });
}

function createRabbitChannel(connection) {
  return new Promise((resolve, reject) => {
    return connection.createConfirmChannel(function (error1, channel) {
      if (error1) {
        reject(error1);
      }
      resolve(channel);
    });
  });
}
