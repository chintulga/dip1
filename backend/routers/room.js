const express = require("express");
const router = express.Router();
const Room = require("../schema/Room");
const User = require("../schema/User");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const config = require("../config");
const uuid = require("uuid");
router.post("/room", async (req, res, next) => {
  try {
    let ourId = [req.body.myId, req.body.hisId];

    let room = await Room.find({
      "users.userId": { $in: ourId },
      isPrivateChat: true,
    });
    console.log(room);
    let groupRoom = await Room.find({
      "users.userId": { $in: ourId },
      isPrivateChat: false,
    });
    let mydoc = await User.find({ _id: { $in: ourId } });
    console.log(mydoc);
    let roomName = mydoc.username + "," + hisdoc.username;
    if (!room) {
      console.log("room not found");
      let users = [];
      users.push(req.body.myId);
      users.push(req.body.hisId);

      let newRoom = new Room({
        roomId: uuid.v1(),
        lastUpdated: req.body.name,
        roomName: roomName,
        isPrivateChat: true,
        users: users,
      });

      newUser.save().catch((err) => {
        console.log(err);
        throw "Failed to register user";
      });
      res.status(200).json({
        success: true,
        msg: "User registered",
      });
    } else {
    }
  } catch (error) {
    res.status(400).json({ success: false, msg: error });
  }
});

router.post("/login", async (req, res, next) => {
  try {
    let email = req.body.email;
    let user = await User.findOne({
      email: email,
    });
    if (!user || !bcrypt.compareSync(req.body.password, user.password)) {
      // authentication failed
      res.status(200).json({
        success: false,
        msg: "Tanii nevtreh ner esvel nuuts ug buruu baina.",
      });
    } else {
      console.log(user._id.valueOf());

      // authentication successful

      const token = jwt.sign(
        { user_id: user._id.valueOf() },
        config.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );
      console.log(req.body);

      // save user token
      user.token = token;

      res.status(200).json({
        success: true,
        msg: "Succesfully logged in",

        token: token,
      });
    }
  } catch (error) {
    res.status(400).json({ success: false, msg: error });
  }
});

module.exports = router;
