import axios from "axios";

axios.defaults.baseURL = "http://localhost:3001/"

// axios.defaults.baseURL = "http://http://172.29.125.172:3001";

interface loginFormType {
  email: string;
  password: string;
}

export const userLogin = (loginForm: loginFormType) => {
  return axios.post("/user/login", loginForm);
};

interface registerFormType {
  name: string;
  email: string;
  username: string;
  password: string;
}

export const userRegister = (registerForm: registerFormType) => {
  return axios.post("/user/register", registerForm);
};

interface searchType {
  keyword: string;
}

export const userSearch = (search: searchType) => {
  return axios.post("/user/search", search);
};
