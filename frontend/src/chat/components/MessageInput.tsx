import { useState, useContext } from "react";
// import './MessageInput.less';
import AudioInput from "./AudioInput";
import { userContext } from "../../UserContext";
const NewMessage = ({ socket, style }: { socket: any; style: any }) => {
  const [value, setValue] = useState("");
  const [showAudio, setAudio] = useState<boolean>(false);
  const { user } = useContext(userContext);

  const onTextChange = (event: { target: { value: string } }) => {
    setValue(event.target.value);
  };

  const handleKeyDown = (event: any) => {
    if (event.key === "Enter") {
      onSendClick();
    }
  };

  //generate uuid
  const uuid = () => {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
      /[xy]/g,
      function (c) {
        var r = (Math.random() * 16) | 0,
          v = c === "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
      }
    );
  };

  const onSendClick = async () => {
    console.log("Sending msg:", value);

    if (user.username && value) {
      const messageString = JSON.stringify({
        messageId: uuid(),
        roomName: "Lonewolf",
        dateCreated: new Date().toISOString(),
        username: user.username,
        roomId: "100001",
        message: value,
        messageType: "text",
        seen: false,
      });

      await socket.emit("message:send", messageString);
    }
    setValue("");

    return;
  };

  return (
    <>
      <div className="border-t-2 border-gray-200 px-4 pt-4 mb-2 sm:mb-0">
        {showAudio ? (
          <AudioInput socket={socket} style={{ margin: "0.5em" }}>
            <button
              type="button"
              value="Text"
              onClick={() => setAudio(false)}
              className="message-send-button"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z"
                  clipRule="evenodd"
                />
              </svg>
            </button>
          </AudioInput>
        ) : (
          <div className="relative flex">
            <span className="absolute inset-y-0 flex items-center">
              <button
                type="button"
                onClick={() => setAudio(true)}
                className="inline-flex items-center justify-center rounded-full h-12 w-12 transition duration-500 ease-in-out text-gray-500 hover:bg-gray-300 focus:outline-none"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  className="h-6 w-6 text-gray-600"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M19 11a7 7 0 01-7 7m0 0a7 7 0 01-7-7m7 7v4m0 0H8m4 0h4m-4-8a3 3 0 01-3-3V5a3 3 0 116 0v6a3 3 0 01-3 3z"
                  />
                </svg>
              </button>
            </span>
            <input
              type="text"
              value={value}
              placeholder="Зурвас бичих"
              onKeyDown={handleKeyDown}
              onChange={onTextChange}
              className="chat-message-input"
            />
            <div className="absolute right-0 items-center inset-y-0 hidden sm:flex">
              <button
                type="button"
                className="inline-flex items-center justify-center rounded-full h-10 w-10 transition duration-500 ease-in-out text-gray-500 hover:bg-gray-300 focus:outline-none"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  className="h-6 w-6 text-gray-600"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13"
                  />
                </svg>
              </button>
              <button
                type="button"
                onClick={onSendClick}
                className="message-send-button"
              >
                <span className="font-bold">Илгээх</span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  className="h-6 w-6 ml-2 transform rotate-90"
                >
                  <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z" />
                </svg>
              </button>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default NewMessage;
