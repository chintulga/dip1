import { useEffect, useState, useRef, useContext } from "react";
import { userContext } from "../../UserContext";
import { Seen } from "./Seen";
import { toast } from "react-toastify";

const b64toBlob = (b64Data: string) => {
  const foo = Buffer.from(b64Data.split(",")[1], "base64");
  return new Blob([foo], { type: 'audio/webm;codecs="opus"' });
};

type messageType = {
  messageId: string;
  dateCreated: string;
  from: string;
  username: string;
  roomId: string;
  message: any;
  messageType: string;
  seen: boolean;
};

const Messages = ({ socket, roomId }: { socket: any; roomId: any }) => {
  const [messages, _setMessages] = useState<messageType[]>([]);
  const messagesRef = useRef<any[]>(messages);
  const [isWindowsInFocus, _setWindowsInFocus] = useState(true);
  const windowsStateRef = useRef<boolean>(isWindowsInFocus);
  const dummy = useRef<HTMLDivElement>(null);
  const { user } = useContext(userContext);

  const setWindowsInFocus = (data: boolean) => {
    windowsStateRef.current = data;
    _setWindowsInFocus(data);
  };

  const setMessages = (data: any[]) => {
    messagesRef.current = data;
    _setMessages(data);
  };

  const messageListener = (messagePayload: string) => {
    const newMessage = JSON.parse(messagePayload);

    // console.log("Different user", newmessage.userId != initialState?.username);
    // if (windowsStateRef.current && newmessage.userId != initialState?.username) {
    //   socket?.emit("seenMessage", [newMessage]);
    // }
    console.log("newMessage", newMessage);

    setMessages([...messagesRef.current, newMessage]);

    if (dummy.current) {
      dummy.current.scrollIntoView({ behavior: "smooth" });
    }
  };

  const lastSeenMessageListener = (lastSeenMessage: any[]) => {
    handleMessageSeen(lastSeenMessage);
  };

  const handleMessageSeen = (seenMessages: any[]) => {
    //add property seen to true in message if id matches
    const newMessages = messagesRef.current.map((message) => {
      for (let i = 0; i < seenMessages.length; i++) {
        if (message.id === seenMessages[i].id) {
          message.seen = true;
        }
      }
      return message;
    });
    setMessages(newMessages);
  };

  const errorListener = (err: any) => {
    toast.error(err);
  };

  const messageReceiveListener = (messagePayload: string) => {
    const messages = JSON.parse(messagePayload);
    console.log("messages", messages);
    // console.log(messages);
    setMessages(messages);
  };

  useEffect(() => {
    socket?.on("message:receive", messageListener);

    socket?.on("message:error", errorListener);

    socket?.on("seenMessageA", lastSeenMessageListener);

    socket?.on("messages:receive", messageReceiveListener);

    return () => {
      socket?.off("message:receive", messageListener);
      socket?.off("message:error", errorListener);
      socket?.off("seenMessageA", lastSeenMessageListener);
      socket?.off("messages:receive", messageReceiveListener);
    };
  }, [socket]);

  useEffect(() => {
    console.log("messages", messages);
  }, [messages]);

  const onFocus = () => {
    console.log("Focus", messages);
    let seenMessages = messagesRef.current.filter((message) => {
      if (message.userId !== user.username) {
        return false;
      }
      return message.seen === false;
    });

    if (seenMessages.length !== 0) {
      socket?.emit("seenMessage", seenMessages);
    }

    setWindowsInFocus(true);
  };

  const onBlur = () => {
    console.log("Blur");

    setWindowsInFocus(false);
  };

  useEffect(() => {
    console.log("hereglegch:", user);
    setWindowsInFocus(true);

    window.addEventListener("focus", onFocus);

    window.addEventListener("blur", onBlur);

    return () => {
      window.removeEventListener("focus", onFocus);
      window.removeEventListener("blur", onBlur);
    };
  }, []);

  useEffect(() => {
    socket?.emit("messages:get", roomId);
  }, [roomId]);

  return (
    <div id="messages" className="messages">
      {messages.map((message: messageType, index: number) => {
        // let audioURL;
        // if (message.messageType === "audio") {
        //   const audioBase64 = message.message;
        //   const audioBlob = b64toBlob(audioBase64);
        //   audioURL = URL.createObjectURL(audioBlob);
        //   return (
        //     <div
        //       key={index}
        //       className={
        //         user.username === message.username
        //           ? "message-user message"
        //           : "message-friend message"
        //       }
        //     >
        //       {message.messageType === "audio" ? (
        //         <audio src={audioURL} controls />
        //       ) : (
        //         <>msg</>
        //       )}
        //     </div>
        //   );
        // }
        if (message.username === user.username) {
          return (
            <div key={index} className="chat-message">
              <div className="flex items-end justify-end">
                <div className="chat-my-message-wrapper">
                <span
                    style={{
                      fontSize: "9px",
                      marginBottom: "-6px",
                      color: "GrayText",
                    }}
                  >
                    sent from : {message.from}
                  </span>
                  <div className="flex">
                    <span className={"chat-my-message -mr-6"}>
                      {message.messageType === "text" ? (
                        message.message
                      ) : (
                        <audio
                          src={URL.createObjectURL(b64toBlob(message.message))}
                          controls
                        />
                      )}
                    </span>
                    <Seen />
                  </div>
                </div>
                <img
                  src="https://avatars.dicebear.com/api/identicon/batnaidan.svg"
                  alt="My profile"
                  className="w-6 h-6 rounded-full order-2"
                />
              </div>
            </div>
          );
        } else {
          return (
            <div key={index} className="chat-message">
              <div className="flex items-end">
                <div className="chat-other-message-wrapper">
                <span
                    style={{
                      fontSize: "9px",
                      marginBottom: "-6px",
                      color: "GrayText",
                    }}
                  >
                    sent from : {message.from}
                  </span>
                  <div>
                    <span className="chat-other-message">
                      {message.messageType === "text" ? (
                        message.message
                      ) : (
                        <audio
                          src={URL.createObjectURL(b64toBlob(message.message))}
                          controls
                        />
                      )}
                    </span>
                  </div>
                </div>
                <img
                  src="https://avatars.dicebear.com/api/identicon/chintulga.svg"
                  alt="My profile"
                  className="w-6 h-6 rounded-full order-1"
                />
              </div>
            </div>
          );
        }
      })}
      <div style={{ float: "left", clear: "both" }} ref={dummy}></div>
    </div>
  );
};

export default Messages;
