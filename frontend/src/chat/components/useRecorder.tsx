import { useEffect, useState } from "react";

const useRecorder = () => {
  const [audioURL, setAudioURL] = useState<string>("");
  const [isRecording, setIsRecording] = useState(false);
  const [audioArrayBuffer, setAudioArrayBuffer] = useState<any>();
  const [audioBlob, setAudioBlob] = useState();
  const [recorder, setRecorder] = useState<any>(null);

  useEffect(() => {
    // Lazily obtain recorder first time we're recording.
    if (recorder === null) {
      if (isRecording) {
        requestRecorder().then(setRecorder, console.error);
      }
      return;
    }

    // Manage recorder state.
    if (isRecording) {
      recorder.start();
    } else {
      recorder.stop();
    }

    // Obtain the audio when ready.
    const handleData = (e: any) => {
      console.log(typeof e.data, e.data);
      setAudioURL(URL.createObjectURL(e.data));
      setAudioArrayBuffer(new ArrayBuffer(e.data));
      setAudioBlob(e.data);
    };

    recorder.addEventListener("dataavailable", handleData);
    return () => recorder.removeEventListener("dataavailable", handleData);
  }, [recorder, isRecording]);

  const startRecording = () => {
    setIsRecording(true);
  };

  const stopRecording = () => {
    setIsRecording(false);
  };

  return [
    audioURL,
    audioArrayBuffer,
    audioBlob,
    isRecording,
    startRecording,
    stopRecording,
  ] as const;
};

async function requestRecorder() {
  const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
  return new MediaRecorder(stream);
}

export default useRecorder;
