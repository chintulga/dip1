import { useContext } from "react";
import { userContext } from "../../UserContext";
import useRecorder from "./useRecorder";

function blobToBase64(blob: any) {
  return new Promise((resolve, _) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result);
    reader.readAsDataURL(blob);
  });
}

type audioInputPropType = {
  socket: any;
  style: any;
  children: any;
};

const AudioInput = ({ socket, style, children }: audioInputPropType) => {
  const { user } = useContext(userContext);
  const [
    audioURL,
    audioArrayBuffer,
    audioBlob,
    isRecording,
    startRecording,
    stopRecording,
  ] = useRecorder();

  console.log("audioURL, typeof audioURL", audioURL, typeof audioURL);

  const onSendClick = async () => {
    console.log(socket.id);

    if (user.username && audioURL) {
      const audioBase64 = await blobToBase64(audioBlob);

      const messageString = JSON.stringify({
        timestamp: new Date().toISOString(),
        user: user.username,
        message: audioBase64,
        messageType: "audio",
      });

      await socket.emit("sendA", messageString);
    }

    return;
  };

  return (
    <div className="flex flex-row items-center">
      {children}
      <audio src={audioURL} controls className="inline flex-grow" />
      <input
        type="button"
        onClick={() => startRecording}
        value="Эхлэх"
        className="message-send-button"
      />
      <input
        type="button"
        onClick={() => stopRecording}
        value="Зогсоох"
        className="message-send-button"
      />
      <button
        type="button"
        onClick={onSendClick}
        className="message-send-button"
      >
        <span className="font-bold">Илгээх</span>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
          className="h-6 w-6 ml-2 transform rotate-90"
        >
          <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z" />
        </svg>
      </button>
    </div>
  );
};

export default AudioInput;
