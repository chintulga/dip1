export default function Header({
  name,
  isActive,
  photoUrl,
}: {
  name: string;
  isActive: boolean;
  photoUrl: string;
}) {
  return (
    <div className="flex sm:items-center justify-between py-3 border-b-2 border-gray-200">
      <div className="relative flex items-center space-x-4">
        <div className="relative">
          <span
            className={`absolute ${
              isActive ? "text-green-500" : "text-gray-400"
            } -right-1 -bottom-1`}
          >
            <svg width={16} height={16}>
              <circle cx={8} cy={8} r={8} fill="currentColor" />
            </svg>
          </span>
          <img src={photoUrl} alt="" className="w-10 h-10 rounded-full" />
        </div>
        <div className="flex flex-col leading-tight">
          <div className="text-2xl mt-1 flex items-center">
            <span className="text-gray-700 mr-3">{name}</span>
          </div>
        </div>
      </div>
      <div className="flex items-center space-x-2"></div>
    </div>
  );
}
