import { useEffect, useState, useContext } from "react";
import "./style.css";
import { io } from "socket.io-client";
import Messages from "./components/Messages";
import MessageInput from "./components/MessageInput";
import Header from "./components/Header";
import { userContext } from "../UserContext";
import Cookies from "universal-cookie";
import { useNavigate } from "react-router-dom";
import Sidebar from "./components/Sidebar";

const Chat = (props: Object) => {
  let navigate = useNavigate();
  const cookies = new Cookies();
  const [selectedUser, setSelectedUser] = useState(null);
  const [socket, setSocket] = useState<object>();
  const { user, setUser } = useContext(userContext);
  const [rooms, setRooms] = useState<Array<any>>([]);

  useEffect(() => {
    let userCookie = cookies.get("user");

    if (userCookie) {
      navigate("/");
    } else {
      navigate("/login");
    }

    const messagesListener = (payload: string) => {
      const rooms = JSON.parse(payload);
      setRooms(rooms);
      console.log("rooms", rooms);
      setSelectedUser(rooms[0].roomId);
    };
    // http://
    const newSocket = io(`http://172.29.125.172:31226`, {
      transports: ["websocket"],
      query: userCookie,
      // query: { username: userCookie.username },
    });

    newSocket?.on("rooms:receive", messagesListener);

    newSocket.on("connect", () => {
      //listens for socket io connection
      console.log("socket.connected", newSocket.id);
      setSocket(newSocket);
    });

    return () => {
      newSocket?.off("rooms:receive", messagesListener);
      newSocket.close();
    };
  }, []);

  useEffect(() => {
    console.log("user", user);
  }, [user]);

  return (
    <>
      {socket ? (
        <div className="w-full flex">
          <Sidebar
            socket={socket}
            selectedUser={selectedUser}
            setSelectedUser={setSelectedUser}
            rooms={rooms}
          />
          <div className="flex-1 p:2 sm:p-4 justify-between flex flex-col h-screen">
            <Header
              name="chintu"
              isActive={true}
              photoUrl="https://avatars.dicebear.com/api/identicon/chintu.svg"
            ></Header>
            <Messages socket={socket} roomId={selectedUser} />
            <MessageInput socket={socket} style={{ margin: "0.5em" }} />
          </div>
        </div>
      ) : (
        <h1 className="text-2xl text-center">
          Not connected to chat service...
        </h1>
      )}
    </>
  );
};

export default Chat;
