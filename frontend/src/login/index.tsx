import React, { useState, useContext, useEffect } from "react";
import { userLogin } from "../services/data";
import Cookies from "universal-cookie";
import { userContext } from "../UserContext";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

export default function Login() {
  const cookies = new Cookies();
  const navigate = useNavigate();
  const { user, setUser } = useContext(userContext);
  const [loginForm, setLoginForm] = useState({
    email: "",
    password: "",
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setLoginForm({
      ...loginForm,
      [e.target.name]: e.target.value,
    });
  };

  const handleLoginClick = () => {
    userLogin(loginForm).then(
      ({ data }) => {
        toast.success(data.msg);
        cookies.set("user", data.user, { path: "/", maxAge: 60 * 60 * 2 });
        setUser(data.user);
        navigate("/");
      },
      (err) => {
        console.error(err);
        toast.error(err.response.data.msg);
      }
    );
  };

  useEffect(() => {
    const userCookie = cookies.get("user");
    if (userCookie) {
      navigate("/");
    } else {
      navigate("/login");
    }
  }, []);

  return (
    <div className="flex items-center justify-center min-h-screen bg-gray-100">
      <div className="px-8 py-6 mt-4 text-left bg-white shadow-lg">
        <h3 className="text-2xl font-bold text-center w-80">Чатын апп</h3>
        <div className="mt-4 w-80">
          <div>
            <label className="block">И-мэйл</label>
            <input
              name="email"
              type="text"
              placeholder="И-мэйл"
              onChange={handleChange}
              className="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
            />
          </div>
          <div className="mt-4">
            <label className="block">Нууц үг</label>
            <input
              name="password"
              type="password"
              placeholder="Нууц үг"
              onChange={handleChange}
              className="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
            />
          </div>
          <div className="flex items-baseline justify-between">
            <button
              className="px-6 py-2 mt-4 text-white bg-blue-600 rounded-lg hover:bg-blue-900"
              onClick={handleLoginClick}
            >
              Нэвтрэх
            </button>
            <a
              href="/register"
              className="text-sm text-blue-600 hover:underline"
            >
              Бүртгүүлэх?
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
