import { useEffect } from "react";
import Cookies from "universal-cookie";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

export default function Logout() {
  const cookies = new Cookies();
  const navigate = useNavigate();

  useEffect(() => {
    // remove user from cookie
    cookies.remove("user");
    toast.info("Амжилттай гарлаа");
    // redirect to login page
    navigate("/login");
  }, []);

  return <></>;
}
