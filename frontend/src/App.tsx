import { useState, useEffect } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import Chat from "./chat";
import Login from "./login";
import Register from "./register";
import Logout from "./logout";
import "./App.css";
import Cookies from "universal-cookie";
import { userContext } from "./UserContext";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const cookies = new Cookies();

export default function App() {
  const [user, setUser] = useState(() => {
    if (cookies.get("user")) {
      return cookies.get("user");
    }
    return null;
  });

  return (
    <userContext.Provider value={{ user, setUser }}>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/" element={<Chat />} />
        <Route path="/register" element={<Register />} />
        <Route path="/logout" element={<Logout />} />
      </Routes>
      <ToastContainer />
    </userContext.Provider>
  );
}
