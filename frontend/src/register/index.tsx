import React, { useEffect, useState } from "react";
import { userRegister } from "../services/data";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import Cookies from "universal-cookie";
const cookies = new Cookies();

export default function Register() {
  const navigate = useNavigate();
  const [registerForm, setRegisterForm] = useState({
    username: "",
    password: "",
    name: "",
    email: "",
  });
  useEffect(() => {
    const userCookie = cookies.get("user");
    if (userCookie) {
      navigate("/");
    }
    //  else {
    //   setUser(userCookie);
    // }
  }, []);
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setRegisterForm({
      ...registerForm,
      [e.target.name]: e.target.value,
    });
  };

  const handleLoginClick = () => {
    userRegister(registerForm).then(
      ({ data }) => {
        console.log(data);
        toast.success(data.msg);
      },
      (err) => {
        toast.error(err.response.data.message);
      }
    );
  };
  return (
    <div className="flex items-center justify-center min-h-screen bg-gray-100">
      <div className="px-8 py-6 mt-4 text-left bg-white shadow-lg">
        <h3 className="text-2xl font-bold text-center">Бүртгүүлэх</h3>
        <div className="mt-4 w-80">
          <div>
            <label className="block">Нэвтрэх нэр</label>
            <input
              name="username"
              type="text"
              placeholder="Нэвтрэх нэр"
              onChange={handleChange}
              className="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
            />
          </div>
          <div className="mt-4">
            <label className="block">Нэр</label>
            <input
              name="name"
              type="text"
              placeholder="Нэр"
              onChange={handleChange}
              className="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
            />
          </div>
          <div className="mt-4">
            <label className="block">И-мэйл</label>
            <input
              name="email"
              type="text"
              placeholder="И-мэйл"
              onChange={handleChange}
              className="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
            />
          </div>
          <div className="mt-4">
            <label className="block">Нууц үг</label>
            <input
              name="password"
              type="password"
              placeholder="Нууц үг"
              onChange={handleChange}
              className="w-full px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
            />
          </div>
          <div className="flex items-baseline justify-between">
            <button
              className="px-6 py-2 mt-4 text-white bg-blue-600 rounded-lg hover:bg-blue-900"
              onClick={handleLoginClick}
            >
              Бүртгүүлэх
            </button>
            <a href="/" className="text-sm text-blue-600 hover:underline">
              Нэвтрэх?
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
