import { createContext } from "react";

const userContext = createContext<{ user: any; setUser: any }>({
  user: {},
  setUser: () => {},
});

export { userContext };
