import React from 'react'
import { useEffect, useState } from 'react';
import {
    TextInput,
  StyleSheet,
  Text,
  Button,
  useColorScheme,
  View,
} from 'react-native';
import { List,Card } from '@ant-design/react-native';
const Item = List.Item;
const Brief = Item.Brief;
const Messages = ({ socket }: { socket: any }) => {
  const [messages, setMessages] = useState<any[]>([]);

  useEffect(() => {
    const messageListener = (messagePayload: string) => {
      console.log('Messge Listerner Working!!!');
      const { timestamp, user, message } = JSON.parse(messagePayload);
      setMessages((prevMessages) => {
        const newMessages = [
          ...prevMessages,
          {
            timestamp,
            user,
            message,
          },
        ];
        return newMessages;
      });
    };
    console.log('socket.changed', socket?.id);

    socket?.on('newMessageA', messageListener);
    // socket.emit('getMessagesA');

    return () => {
      socket?.off('newMessageA', messageListener);
    };
  }, [socket]);

  console.log(messages);

  return (
    <View>
      {messages.map(
        ({ timestamp, user, message }: { timestamp: string; user: string; message: string }) => {
          return (
                 <View style={styles.listView} key={timestamp}> 
                {/* <List  key={timestamp}>
                   <Text style={styles.texts}>Username: {user}</Text>
                   <Text style={styles.texts}>Message: {message}</Text>
                   <Text style={styles.texts}>Date: {new Date(timestamp).toLocaleTimeString()}</Text>
                </List> */}
                  <Card>
            <Card.Header
              title={`Username:   ${user}`}
            />
            <Card.Body>
              <View style={{ height: 42 }}>
                <Text style={{ marginLeft: 16 }}>{message}</Text>
              </View>
            </Card.Body>
            <Card.Footer
              content="Sent at:"
              extra={`${timestamp}`}
            />
          </Card>
                 </View>
            // <Row
            //   key={timestamp}
            //   className="message-container"
            //   gutter={[16, 16]}
            //   // title={`Sent at ${new Date(message.time).toLocaleTimeString()}`}
            // >
            // </Row>
          );
        },
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  listView: {
    margin: 12,
    borderRadius: 50,
    backgroundColor: 'red'
  },
  texts: {
    marginTop: 12
  }
});

export default Messages;
