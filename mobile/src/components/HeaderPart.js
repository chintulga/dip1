 import React from 'react';
 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   useColorScheme,
   View,
 } from 'react-native';
 
 import {
   Colors,
   DebugInstructions,
   LearnMoreLinks,
   ReloadInstructions,
 } from 'react-native/Libraries/NewAppScreen';
 

 
 const HeaderPart: () => Node = () => {
   return (
    <View style={styles.sectionContainer}>
      <Text style={styles.sectionTitle}>Chat Demo App</Text>
   </View>
   );
 };
 
 const styles = StyleSheet.create({
   sectionContainer: {
     backgroundColor: Colors.lighter,
     paddingHorizontal: 24,
     height: 100
   },

   sectionTitle: {
     marginTop: 35,
     marginLeft:70,
     fontSize: 24,
     fontWeight: '600',
   },
 
 });
 
 export default HeaderPart;
 