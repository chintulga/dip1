import React  from 'react';
import {useState, useEffect} from 'react'
import {
    TextInput,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button
} from 'react-native';
// import {Button} from '@ant-design/react-native';
const TextField= ({ socket }: { socket: any })=> {
  const [value, setValue] = useState('');
  const [name, setName] = useState('');
 

  const onTextChange = (event: { target: { value: string } }) => {
    setValue(event);
  };


  const onNameChange = (event: { target: { value: string } }) => {
    setName(event);
  };

  const onSendClick = async () => {
    console.log(socket.id);
    const messageString = JSON.stringify({
       timestamp: new Date().toISOString(),
       user: name,
      message: value,
    });

    console.log('messageString===',messageString);
    await socket.emit('sendA', messageString);
    setValue('');
    return;
  };  

  return (
   <View>
     <TextInput style={styles.nameField} placeholder='NAME' onChangeText={onNameChange}/>
      <TextInput
        style={styles.textFieldinput}
        placeholder='TEXT'
        onChangeText={onTextChange}
    
      />
      <View style={styles.submitButton}>
      <Button
      color='black'
        title='Send'
         onPress={onSendClick}
      />
      </View>
          
   </View>
  );
};

const styles = StyleSheet.create({
    textFieldinput: {
        height: 100,
        margin: 12,
        borderWidth: 0.5,
        padding: 10,
      },
      nameField: {
        height: 40,
        margin: 12,
        borderWidth: 0.5,
        padding: 10,
      },
      submitButton: {
         backgroundColor: '#e7e7e7',
         height: '10px',
          margin: 12,
          borderWidth: 1,
          borderRadius: 10,
          marginLeft: 60,
          marginRight: 60,
        },
});

export default TextField;
