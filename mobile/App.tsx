import React from 'react';
import {useState, useEffect} from 'react'
import { Text, View, ScrollView,SafeAreaView } from 'react-native';
import {
  Colors,
  DebugInstructions,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import io from "socket.io-client/dist/socket.io";
import HeaderPart from './src/components/HeaderPart'
import TextField from './src/components/TextField';
import Message from './src/components/Message';
import CONFIG from './config';

export default (props: Object) => {
   const [socket, setSocket] = useState<object>();

   useEffect(() => {
    const newSocket = io(`http://${CONFIG.hostname}:3456`);
    console.log('socket.connected', newSocket.id);
    setSocket(newSocket);
    return () => {
      newSocket.close();
    };
  }, [setSocket]);
  return (
    <SafeAreaView>
  <ScrollView>
    <HeaderPart/>
    <View style={{ background: 'white', padding: '24px', borderRadius: '4px' }}>
        {socket && (
          <>
           <View>
              <Message socket={socket} />
            </View>
          </>
        )}
      </View>
    <TextField socket={socket} />
  </ScrollView>
  </SafeAreaView>
  );
};




